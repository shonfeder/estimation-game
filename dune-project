(lang dune 2.7)
(name estimation-game)

(generate_opam_files true)

(source (uri "git://gitlab.com:shonfeder/estimation-game.git"))
(license MIT)
(authors "Shon Feder" "Ian Thacker")
(maintainers "shon.feder@gmail.com")
(homepage "https://gitlab.com/shonfeder/estimation-game")
(bug_reports "https://gitlab.com/shonfeder/estimation-game/-/issues")

(package
 (name estimation-game)
 (synopsis "A web app for slamming conceptual frameworks with facts")
 (description "A front-end web app enabling generation of estimation games that
help players learn about the world and transform their concepts.")
 (depends
  ; build
  (ocaml (>= 4.12))
  (dune (>= 2.7))

  ; libs
  (bonsai (and (= v0.14.0) (< v0.15.0)))
  (tyxml (and (>= 4.4.0) (< 5.0.0)))
  (dream (= 1.0.0~alpha1))
  (csv-lwt (>= 2.4))
  (bos (>= 0.2.0))

  ; preprocessing
  (tyxml-ppx (and (>= 4.3.0) (< 5.0.0)))

  ; test
  (ppx_inline_test :with-test)))
