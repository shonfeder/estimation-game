open! Lib

let get_param req param = Dream.query param req |> Option.value ~default:""

(* We don't know what the following params are for, but Qualtrics asked
 *  for them:
 * - p
 * - vid
 * - id1
 * - id2
 * - id3 *)
let condition route (module C : Config.S) =
  let module Tmpl = Template.Condition.Make (C) in
  Dream.get route (fun req ->
      let query = get_param req "uuid" in
      let p = get_param req "P" in
      let vid = get_param req "VID" in
      let id1 = get_param req "ID1" in
      let id2 = get_param req "ID2" in
      let id3 = get_param req "ID3" in
      Dream.html Tmpl.(html query p vid id1 id2 id3))

let handle_result push_report request =
  match Dream.query "data" request with
  | None   ->
      Dream.warning (fun l -> l ~request "no data sent in result request");
      Dream.json {|{"status": "no result supplied"}|}
  | Some r ->
      Dream.debug (fun l -> l ~request "received result %s" r);
      let () =
        match Report.of_string r with
        | Ok r             -> push_report (Some r)
        | Error (`Msg err) ->
            Dream.error (fun l -> l "could not deserialize report %s" err)
      in
      Dream.json (Printf.sprintf {|"status": "result received: %s"|} r)

let log_level_of_string = function
  | "debug" -> `Debug
  | "info"  -> `Info
  | "warn"  -> `Warning
  | "error" -> `Error
  | _       -> failwith "Invalid log level"

let log_level_to_string = function
  | `Debug   -> "debug"
  | `Info    -> "info"
  | `Warning -> "warn"
  | `Error   -> "error"

let get_log_level () =
  let default = `Warning in
  "LOG_LEVEL"
  |> Sys.getenv_opt
  |> Option.map log_level_of_string
  |> Option.value ~default

let server ~level =
  let open Lwt.Syntax in
  Dream.initialize_log ~level ();
  Dream.info (fun l -> l "Logging level set to %s" (log_level_to_string level));
  let port, interface = Server_info.(port, host) in
  Dream.info (fun l -> l "Serving %s:%i" interface port);
  let* report_writer = Reporter.stream_writer () in
  Dream.serve
    ~interface
    ~port
    ~error_handler:(Dream.error_template Template.error)
  @@ Dream.logger
  @@ Dream.router
       [ Dream.get "/static/**" (Dream.static "./_build/default/static")
       ; Dream.get "/" (fun _ -> Dream.html Template.index)
       ; Dream.get "/result" (handle_result report_writer)
       ; condition "/-1" (module Config.Example)
       ; condition "/0" (module Config.Control)
       ; condition "/1" (module Config.Climeate_change_quantities)
       ; condition "/2" (module Config.Modification)
       ]
  @@ Dream.not_found

let () =
  let level = get_log_level () in
  Lwt_main.run @@ server ~level
