open Lib

module Condition = struct
  module Make (C : Config.S) = struct
(* TODO Move into separate eml.ml functr module and then import here *)

        (* We don't know what the following params are for, but Qualtrics asked
         *  for them:
         * - p
         * - vid
         * - id1
         * - id2
         * - id3 *)
        let html uuid p vid id1 id2 id3 =
          <html>
              <head>
                  <meta charset="UTF-8">
                  <meta name="viewport" content=
                  "width=device-width, initial-scale=1.0">
                  <title><%s C.title %></title>
                  <script defer src="/static/<%s C.name %>/main.bc.js"></script>
                  <link href="https://unpkg.com/nes.css@2.3.0/css/nes.min.css" rel="stylesheet">
                  <!-- Font -->
                  <link rel="preconnect" href="https://fonts.gstatic.com">
                  <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&family=Quicksand&display=swap"
                        rel="stylesheet">
                  <!-- Custom CSS -->
                  <link rel="stylesheet" href="/static/style.css">
              </head>
              <body>
                  <div id="backend-data" style="display:none">
                    <span id="uuid"><%s uuid %></span>
                    <span id="p"><%s p %></span>
                    <span id="vid"><%s vid %></span>
                    <span id="id1"><%s id1 %></span>
                    <span id="id2"><%s id2 %></span>
                    <span id="id3"><%s id3 %></span>
                  </div>
                  <div id="app"></div>
              </body>
          </html>
    end
end

let index =
    <!DOCTYPE html>
    <!-- Comment may be needed for responsive styling on some browsers -->
    <html>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content=
            "width=device-width, initial-scale=1.0">
            <title>Estimation Game</title>
            <!-- Font -->
            <link href="https://unpkg.com/nes.css@2.3.0/css/nes.min.css"
                  rel= "stylesheet">
            <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&amp;display=swap"
                  rel="stylesheet">
            <!-- Custom CSS -->
            <link rel="stylesheet" href="/static/style.css">
        </head>
        <body>
            <div id="container" class="nes-container with-title">
                <h1 class="title">Estimation Game</h1>
                <h2>Experimental Conditions</h2>
                <ol>
                <li>
                    <a href="./-1">Example</a>
                </li>
                <li>
                    <a href="./0">Control</a>
                </li>
                <li>
                    <a href="./1">Intervention</a>
                </li>
                <li>
                    <a href="./2">Intervention with modification</a>
                </li>
                </ol>
            </div>
        </body>
    </html>

(* See https://github.com/aantron/dream/tree/master/example/9-error#files *)
let error debug_info suggested_response =
  let status = Dream.status suggested_response in
  let code = Dream.status_to_int status
  and reason = Dream.status_to_string status in

  suggested_response
  |> Dream.with_header "Content-Type" Dream.text_html
  |> Dream.with_body begin
    <html>
      <body>
        <h1><%i code %> <%s reason %></h1>

%       begin match debug_info with
%       | None -> ()
%       | Some debug_info ->
          <pre><%s debug_info %></pre>
%       end;

      </body>
    </html>
  end
  |> Lwt.return
