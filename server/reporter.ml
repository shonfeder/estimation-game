(** Reporter records results from participant answers *)

open! Core_kernel
open Lib

module Row = struct
  type t =
    { condition : string
    ; uuid : string option
    ; question_id : string
    ; prompt : string
    ; practice_question : bool
    ; actual_quantity : Value.Quantity.t
    ; actual_direction : Value.Change.t option
    ; evaluation : Evaluation.t
    ; estimated_quantity : Value.Quantity.t
    ; estimated_direction : Value.Change.t option
    }

  let of_report (r : Report.t) =
    { condition = r.condition
    ; uuid = r.uuid
    ; question_id = r.question.id
    ; prompt = String.concat ~sep:"\n" r.question.prompt
    ; practice_question = r.question.practice
    ; actual_quantity = r.question.value.quantity
    ; actual_direction = r.question.value.change
    ; evaluation = r.evaluation
    ; estimated_quantity = r.estimate.quantity
    ; estimated_direction = r.estimate.change
    }

  let opt_change_to_string = function
    | None   -> ""
    | Some r -> Value.Change.to_string r

  let header =
    [ "condition"
    ; "participant_uuid"
    ; "question_id"
    ; "prompt"
    ; "practice_question"
    ; "actual_quantity"
    ; "actual_direction"
    ; "evaluation"
    ; "estimated_quantity"
    ; "estimated_direction"
    ]

  let to_strings t =
    [ t.condition
    ; Option.value t.uuid ~default:""
    ; t.question_id
    ; t.prompt
    ; Bool.to_string t.practice_question
    ; Value.Quantity.to_string t.actual_quantity
    ; opt_change_to_string t.actual_direction
    ; Evaluation.to_string t.evaluation
    ; Value.Quantity.to_string t.estimated_quantity
    ; opt_change_to_string t.estimated_direction
    ]
end

let file = Printf.sprintf "_results/%s.csv" Lib.version

let write_row oc row = Csv_lwt.(output_record (to_channel oc)) row

(** Writes header on file if it doesn't exist *)
let ensure_file_exists () =
  let open Lwt.Syntax in
  let* file_exists = Lwt_unix.file_exists file in
  let+ () =
    if file_exists then
      Lwt.return ()
    else
      let* dir_exists = Lwt_unix.file_exists "_results" in
      let* () =
        if not dir_exists then
          Lwt_unix.mkdir "_results" 0o755
        else
          Lwt.return ()
      in
      Lwt_io.with_file ~mode:Lwt_io.output file (fun oc ->
          write_row oc Row.header)
  in
  Dream.info (fun l -> l "Results will be written to %s" file)

let write_report : Report.t -> Lwt_io.output_channel -> unit Lwt.t =
 fun report oc ->
  (* We are not using the ppx_csv_conv writer directly because it doesn't support async writes *)
  Row.of_report report |> Row.to_strings |> write_row oc

let write_report_to_file : file:string -> Report.t -> unit Lwt.t =
 fun ~file report ->
  Lwt_io.with_file
    ~flags:UnixLabels.[ O_APPEND; O_WRONLY; O_NONBLOCK ]
    ~mode:Lwt_io.output
    file
    (write_report report)

(** [stream_writer ~file] is a "push" function, used to add new records
    to an internal stream. Each records added to the stream is appended to [file].
    This provides a synchronization point for concurrent writes to [file]. *)
let stream_writer : unit -> (Report.t option -> unit) Lwt.t =
 fun () ->
  let strm, push = Lwt_stream.create () in
  let (_ : unit Lwt.t) = Lwt_stream.iter_s (write_report_to_file ~file) strm in
  let open Lwt.Syntax in
  let* () = ensure_file_exists () in
  Lwt.return push
