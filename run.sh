#!/usr/bin/env bash
set -euo pipefail

dune build && dune exec server/server.exe 2> >(tee -a server.log >&2)
