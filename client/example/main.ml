open! Core_kernel
open! Bonsai_web
module Impl = Lib.Impl.Make (Lib.Config.Example)

let (_ : _ Start.Handle.t) =
  Start.(
    start_standalone
      ~initial_input:()
      ~bind_to_element_with_id:"app"
      Impl.application)
