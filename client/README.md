Each subdirectory corresponds to an experimental condition.

Each condition is derived by applying a functor to a config module. Each
conditions is compiled to JavaScript. Because each condition has its own
executable, but the build logic of each is the same, we define the build logic
in the `dune.inc` file, and then include this in the dune file for each
condition.
