#!/usr/bin/env bash
set -euo pipefail

set -x

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
ROOT="${SCRIPTPATH}/../.."

source "${SCRIPTPATH}/config.sh"

cd "$ROOT"

opam show ./estimation-game.opam --field=depends: \
    | awk '{ print $1 }' | xargs opam -y depext
opam install . --locked --yes --deps-only
opam exec dune -- build --profile release

# Server executable
cp -f _build/default/server/server.exe .

# Report update executable
cp -f _build/default/util/git.exe .
