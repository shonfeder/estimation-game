#!/usr/bin/env bash
set -euo pipefail

set -x

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
ROOT="${SCRIPTPATH}/../.."

source "${SCRIPTPATH}/config.sh"

cd "$ROOT"

rsync -rlv . build@$SERV:app --exclude _build --exclude _opam --exclude _results --exclude server.exe
ssh build@$SERV "cd app && bash deploy/script/build.sh"
ssh root@$SERV "bash /home/build/app/deploy/script/install.sh"
