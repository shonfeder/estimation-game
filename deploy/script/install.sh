#!/usr/bin/env bash
set -euo pipefail

set -x

if ! [[ $(hostname) == "estimation-game" ]]
then
    echo "Not running on target host: aborting install"
    exit 1
fi

cp /home/build/app/deploy/systemd/git.service /etc/systemd/system/
cp /home/build/app/deploy/systemd/git.timer /etc/systemd/system/
cp /home/build/app/deploy/systemd/app.service /etc/systemd/system/

systemctl daemon-reload
systemctl restart app

systemctl enable git.timer
systemctl restart git.timer
