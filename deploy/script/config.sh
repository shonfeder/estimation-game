#!/usr/bin/env bash
set -euo pipefail

# Build-time configuration for deploying the server

export SERV=143.110.210.183
export HOST=0.0.0.0
export PORT=80
