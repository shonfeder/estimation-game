# Deployment

This directory holds scripts and systemd units used for deploying the
application.

The deployment scheme is adapated from
https://github.com/aantron/dream/tree/master/example/z-systemd .

## System requirements

- bubblewrap
- cc
- libev4
- m4
- make
- systemd
- unzip
- opam

Only tested on Arch and Ubuntu.

## Manual deployment

From the root directory 

```sh
$ ./deploy/script/deploy.sh
```

You must have SSH access to the `HOST` specified in that script.
