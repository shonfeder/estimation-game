# Use opam for a fresh build
FROM ocaml/opam:ubuntu-18.04-ocaml-4.12
# Inherits any deps used in previous successful merges to master
# FROM registry.gitlab.com/shonfeder/estimation-game/deps
MAINTAINER Shon Feder <shon.feder@gmail.com>

ADD ./estimation-game.opam .

RUN sudo apt update

# We need nodejs for jsoo tests
RUN sudo apt -y install nodejs

# TODO Remove once using opam >= 2.1
# Download system dependencies
RUN opam show ./estimation-game.opam --field=depends: \
    | awk '{ print $1 }' | xargs opam depext

# Install the dependencies
RUN opam install ./estimation-game.opam --yes --deps-only --with-test
