# Estimation Game

This is a web application to used as an intervention in a study on effecting
conceptual change regarding the severity of climate change through numerical
estimation exercises. The study was designed and lead by [Ian
Thacker](http://ianthacker.com). We have a [draft reporting the
findings](http://ianthacker.com/PME-NA%202021_SLAMMER_FINAL_R1.pdf) available.

The frontend of the app is available [here](https://shonfeder.gitlab.io/estimation-game/2/index.html).

For the project status and development roadmap, see...

- [milestones][]
- [open issues][]
- [design notes][]

[milestones]: https://gitlab.com/shonfeder/estimation-game/-/milestones
[open issues]: https://gitlab.com/shonfeder/estimation-game/-/issues
[design notes]: ./design-notes.org

## Code organization

- [lib/](./lib): Defines shared code that can be used by the client-side and backend.
- [client/](./client): Defines the entrypoints for client side code.
- [static/](./static): Contains (or specifies) the static assets served.
- [server/](./server): Defines the server.
- [dune-project](./dune-project): Specifies the package and dependencies.
- [.gitlab-ci](./.gitlab-ci): Specifies the CI configuration.
- [Dockerfile](./Dockerfile): Used for dockerizing dependencies in CI.

## Architecture

This is a full stack OCaml application.

### Client

Client-side code is written using [bonsai](https://github.com/janestreet/bonsai)
(version `0.14`).

The client-side code for each experimental condition is implemented as a
reactive, single page application. All conditions share the same logic, specified
by the `Make` functor in the `Component` module in the `lib` directory. 

The different condition configurations are specified in the `Config` module in
the `lib` directory.

### Server

Server-side code is written using [dream](https://github.com/aantron/dream)
(version `1.0.0~alpha1`).

The server-side code for each experimental condition is implemented through
a `Dream` HTML template. All conditions share the same logic, specified by the
`Make` functor in the `Template` module in the `server` directory.

### Database

Each question answered by a participant is appended to a CSV file,
`_results/<version>.csv`, where `<version>` is the current version of the
running application.

These results are synced periodically to
https://gitlab.com/conceptual-change-research/estimation-game-results via a
small utility, defined in `util/git.ml`, executed as a scheduled systemd
service.

## Development

### Requirements

- [opam](https://opam.ocaml.org/doc/Install.html)

### Dependencies

To set up a sandbox with all dependencies installed, run

``` sh
$ opam switch create . --with-test --deps-only
```

### Locking the dependencies

``` sh
$ opam lock
```
### Running the server

``` sh
$ run.sh
```

This will log to `server.log` as well as to stderr.

The environment variable `LOG_LEVEL` can be set at run time to change the
logging verbosity. It defaults to `warn`.

### Configuration

The following environment variables can be used to configure the obvious parameters **at build time**:

- `PORT` (defaults to `8080`)
- `HOST` (defaults to `"localhost"`)
 
E.g., to build the server to serve `0.0.0.0` on port `80` you can do

```sh

HOST=0.0.0.0 PORT=80 dune build
```

### CI

#### Dependencies

We currently build the CI dependencies in a docker image, which we then host on the GitLab container registry for this project.

##### Update the dependencies

In the `Dockerfile`, comment out the `FROM` line specifying the registry:


``` dockerfile
# FROM registry.gitlab.com/shonfeder/estimation-game/deps
```
 
and uncomment the `FROM` line specifying the upstream opam image:

``` dockerfile
FROM ocaml/opam:alpine-ocaml-4.11
```

Push your branch. This should trigger a new update to the image at https://gitlab.com/shonfeder/estimation-game/container_registry/1634460.

<!-- Then build a fresh image: -->

<!-- ``` sh -->
<!-- $ docker build . -->
```


## Deployment

Deployment follows the example from https://github.com/aantron/dream/tree/master/example/z-systemd

See [./deloy/README.md](./deloy/README.md) for details.
