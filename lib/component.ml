(** The DOM component encapuslating the game *)

open! Core_kernel
open Bonsai_web

let ( let+ ) = Option.Monad_infix.( >>| )

let ( let* ) = Option.Monad_infix.( >>= )

let estimate_id = "estimate"

let value_class = "value"

let quantity_id = "quantity-input"

let change_id = "change-input"

let instruction_class = "instruction"

let cons_opt : 'a option -> 'a list -> 'a list =
 fun x xs ->
  match x with
  | None   -> xs
  | Some x -> x :: xs

let scroll_to_top () =
  let open Js_of_ocaml in
  Dom_html.window##scroll 0 0

(** [VNode] *)
module VNode = struct
  let tex_field ?(attrs = []) ?id ?(placeholder = "") () =
    let open Vdom in
    let attrs =
      match id with
      | None    -> attrs
      | Some id -> Attr.id id :: attrs
    in
    Node.input
      (attrs
      @ [ Attr.string_property "placeholder" placeholder; Attr.type_ "text" ])
      []

  let fieldset attrs label fields =
    let open Vdom in
    let legend = Node.label [ Attr.for_ quantity_id ] [ Node.text label ] in
    Node.div
      (Attr.classes [ "nes-field"; "quantity-fields" ] :: attrs)
      (legend :: fields)

  let selector ?(attrs = []) ?placeholder ~id options () =
    let open Vdom in
    let option t = Node.option [ Attr.value t ] [ Node.text t ] in
    let placeholder =
      match placeholder with
      | None   -> []
      | Some p ->
          [ Node.option
              [ Attr.value ""; Attr.disabled; Attr.selected ]
              [ Node.text p ]
          ]
    in
    Node.select
      (attrs @ [ Attr.id id ])
      (placeholder @ List.map ~f:option options)

  let details ?(attrs = []) ~summary details =
    let open Vdom in
    let summary = Node.create "summary" [] [ Node.text summary ] in
    Node.create "details" attrs (summary :: details)

  let progress ~max ~value =
    let open Vdom in
    let label =
      Node.label [ Attr.for_ "game-progress" ] [ Node.text "Progress..." ]
    in
    let bar =
      let content = Printf.sprintf "%d/%d" value max in
      let attrs =
        [ Attr.id "game-progress"
        ; Attr.classes [ "nes-progress"; "is-primary" ]
        ; Attr.value (Int.to_string value)
        ; Attr.max (Float.of_int max)
        ]
      in
      Node.create "progress" attrs [ Node.text content ]
    in
    Node.div [ Attr.class_ "progress-bar" ] [ label; bar ]

  let empty_star size =
    let open Vdom in
    Node.create "i" [ Attr.classes [ "nes-icon"; size; "star"; "is-empty" ] ] []

  let full_star size =
    let open Vdom in
    Node.create "i" [ Attr.classes [ "nes-icon"; size; "star" ] ] []

  let eval_stars ~size eval =
    let max = Evaluation.(to_int max) in
    let open Vdom in
    let full = Evaluation.to_int eval in
    let empty = max - full in
    Node.div
      [ Attr.class_ "star-rating" ]
      (List.init full ~f:(fun _ -> full_star size)
      @ List.init empty ~f:(fun _ -> empty_star size))
end

module Make (Config : Config.S) :
  Bonsai.S
    with module Model = Model
     and module Input = Unit
     and type Result.t = Model.t * Vdom.Node.t = struct
  let name = "Estimation_component"

  module Input = Unit
  module Model = Model

  module Result = struct
    type t = Model.t * Vdom.Node.t
  end

  module Action = struct
    type t =
      | Next
      | Answer of Value.t
      | Invalid_input of Invalid.t
    [@@deriving sexp]
  end

  let condition_id =
    match Config.name with
    | "example"      -> "-1"
    | "control"      -> "0"
    | "intervention" -> "1"
    | "modification" -> "2"
    | _              -> "UNKNOWN CONDITION"

  let log str = Js_of_ocaml.(Firebug.console##log Js.(string str))

  let apply_invalid model : Invalid.t -> Model.t =
   fun invalid ->
    let msg = Invalid.to_string invalid in
    log ("invalid value input: " ^ msg);
    { model with input_err = Option.some msg }

  let get_backend_data key =
    let open Js_of_ocaml in
    let+ elem = Dom_html.getElementById_coerce key Dom_html.CoerceTo.element in
    Js.to_string elem##.innerHTML

  let add_backend_data : Model.t -> Model.t =
   fun model ->
    (* This should only be None on the first iteration *)
    if Option.is_some model.uuid then
      model
    else
      let uuid = get_backend_data "uuid" in
      let qualtrics_data : Model.qualtrics_data =
        { p = get_backend_data "p"
        ; vid = get_backend_data "vid"
        ; id1 = get_backend_data "id1"
        ; id2 = get_backend_data "id2"
        ; id3 = get_backend_data "id3"
        }
      in
      { model with uuid; qualtrics_data }

  let apply_action ~inject:_ ~schedule_event:_ :
      Input.t -> Model.t -> Action.t -> Model.t =
   fun () model action ->
    (* TODO Find better way of injecting this into the model *)
    let model = add_backend_data model in
    match action with
    | Action.Next -> Model.step model
    | Action.Answer estimate ->
        Model.record_estimate
          Config.name
          estimate
          { model with input_err = None }
    | Action.Invalid_input invalid -> apply_invalid model invalid

  let get_estimate : Question.t -> (Value.t, Invalid.t) result =
    let open Js_of_ocaml in
    fun q ->
      let get_dir () =
        let* elem =
          Dom_html.getElementById_coerce change_id Dom_html.CoerceTo.select
        in
        Js.to_string elem##.value |> Value.Change.of_string
      in
      let value () =
        let+ elem =
          Dom_html.getElementById_coerce quantity_id Dom_html.CoerceTo.input
        in
        Js.to_string elem##.value
      in
      match value () with
      | None   -> Error Invalid.No_value
      | Some v -> (
          let ( let* ) = Core_kernel.Result.Monad_infix.( >>= ) in
          let* estimate =
            match q.value.quantity with
            | Value.Quantity.Float _ -> Value.float_of_string v
            | Value.Quantity.Int _   -> Value.int_of_string v
          in
          match q.value.change with
          | None   -> Ok estimate
          | Some _ ->
          match get_dir () with
          | None     -> Error Invalid.No_direction
          | Some dir -> Ok (estimate |> Value.with_change dir))

  let btn_classes = Vdom.Attr.classes [ "nes-btn"; "is-primary" ]

  let enter_button inject q =
    let open Vdom in
    let on_click =
      Attr.on_click @@ fun _ ->
      let action =
        match get_estimate q with
        | Ok estimate -> Action.Answer estimate
        | Error err   -> Action.Invalid_input err
      in
      inject action
    in
    Node.button [ btn_classes; on_click ] [ Node.text "Enter" ]

  let next_button inject =
    let open Vdom in
    let on_click = Attr.on_click (fun _ -> inject Action.Next) in
    Node.button [ btn_classes; on_click ] [ Node.text "Next" ]

  let input_classes = Vdom.Attr.classes [ "nes-input"; "quantity" ]

  (** When called with a fixed value it is disabled *)
  let quantity_input ?value ?name () =
    let open Vdom in
    let name = Option.value name ~default:quantity_id |> Attr.name in
    let attrs =
      match value with
      | None   ->
          [ Attr.placeholder "Quantity"
          ; Attr.id quantity_id
          ; name
          ; Attr.autofocus true
          ]
      | Some v -> [ Attr.value v; Attr.disabled ]
    in
    Node.input ([ input_classes; Attr.type_ "number" ] @ attrs) []

  (* TODO Make select box not take up whole space *)
  let change_input ?(classes = []) ?value () =
    let open Vdom in
    let classes = Attr.classes ("change" :: classes) in
    let select_div select =
      Node.span [ Attr.classes [ "nes-select" ] ] [ select ]
    in
    select_div
    @@
    match value with
    | None   ->
        let placeholder =
          Node.option
            [ Attr.value ""; Attr.disabled; Attr.selected ]
            [ Node.text "Choose..." ]
        in
        let option t = Node.option [ Attr.value t ] [ Node.text t ] in
        Node.select
          [ Attr.id change_id; classes ]
          [ placeholder; option "increase"; option "decrease" ]
    | Some d ->
        let dir = Value.Change.to_string d in
        Node.select
          [ Attr.id change_id; classes; Attr.disabled ]
          [ Node.option [ Attr.value dir; Attr.selected ] [ Node.text dir ] ]

  (* let eval_to_stars : size:string -> Evaluation.t -> Vdom.Node.t =
   *  fun ~size eval ->
   *   let n = Evaluation.to_int eval in
   *   VNode.eval_stars ~size n *)

  let view_value ?eval label (question : Question.t) (value : Value.t) =
    let open Vdom in
    let classes = Attr.classes [ value_class; label ] in
    let estimate =
      let quantity =
        let v = Value.Quantity.to_string value.quantity in
        quantity_input ~value:v ()
      in
      let units = Node.text [%string " %{question.units} "] in
      let change =
        let input_classes =
          match eval with
          | Some Evaluation.Wrong_dir -> [ "eval-wrong-dir" ]
          | _                         -> []
        in
        match value.change with
        | None   -> []
        | Some v -> [ change_input ~classes:input_classes ~value:v () ]
      in
      VNode.fieldset [] label [ Node.span [] ([ quantity; units ] @ change) ]
    in
    Node.div [ classes ] [ estimate ]

  let input_value inject (question : Question.t) =
    let open Vdom in
    let estimate =
      let quantity = quantity_input () in
      let units = Node.text [%string " %{question.units} "] in
      let change =
        match question.value.change with
        | None   -> []
        | Some _ -> [ change_input () ]
      in
      VNode.fieldset
        []
        "Estimate"
        [ Node.span [] ([ quantity; units ] @ change) ]
    in
    let enter_button = Node.p [] [ enter_button inject question ] in
    Node.div [ Attr.class_ value_class ] [ estimate; enter_button ]

  let view_question (q : Question.t) =
    let open Vdom in
    let para t = Node.p [] [ Node.text t ] in
    let prompt =
      Node.div [ Attr.class_ instruction_class ] (List.map ~f:para q.prompt)
    in
    let hint =
      match q.hint with
      | None   -> []
      | Some h ->
          [ Node.p
              [ Attr.class_ instruction_class ]
              [ Node.create "em" [] [ Node.text [%string "(Hint: %{h})"] ] ]
          ]
    in
    Node.div [ Attr.id "question" ] (prompt :: hint)

  let view_input_err = function
    | None     -> []
    | Some err -> Vdom.[ Node.p [ Attr.class_ "input-err" ] [ Node.text err ] ]

  let view_prompt inject input_err (q : Question.t) =
    let open Vdom in
    Node.div
      []
      ([ view_question q; input_value inject q ] @ view_input_err input_err)

  let element_of_citation : Question.citation -> Vdom.Node.t =
   fun { name; url } ->
    let open Vdom in
    let name_txt = Node.text name in
    match url with
    | None     -> name_txt
    | Some url ->
        Node.a [ Attr.href url; Attr.create "target" "_blank" ] [ name_txt ]

  let view_citations : Question.citation list -> Vdom.Node.t list =
   fun cites ->
    let open Vdom in
    let citations =
      let f c = c |> element_of_citation |> List.return |> Node.li [] in
      Node.ul [ Attr.classes [ "citations-list" ] ] (List.map ~f cites)
    in
    [ VNode.details ~summary:"Sources" [ citations ] ]

  let view_context Question.{ context; _ } =
    match context with
    | None               -> []
    | Some (info, cites) ->
        Vdom.
          [ Node.p
              [ Attr.class_ "context" ]
              [ Node.p [] (Node.text info :: view_citations cites) ]
          ]

  let send_report (report : Report.t) =
    let open Js_of_ocaml in
    (* TODO Use Async solution native to bonsai or Js_of_ocaml LWT? *)
    (* See https://github.com/whitequark/js_of_ocaml-example/blob/master/src/ex1.ml*)
    let report_ser =
      Report.sexp_of_t report |> Sexp.to_string_mach |> Url.urlencode
    in
    let req =
      Printf.sprintf
        "http://%s:%i/result?data=%s"
        Server_info.serv
        Server_info.port
        report_ser
    in
    let xmlhttp = XmlHttpRequest.create () in
    xmlhttp##_open (Js.string "GET") (Js.string req) Js._true;
    (* TODO Check the status to log error in case request is not replied to? *)
    xmlhttp##send Js.null

  (* View the report on the estimation compared against the actual value *)
  let view_report inject (report : Report.t) =
    let open Vdom in
    (* AJAX request to send result to backend *)
    let () = send_report report in
    let question = report.question in
    let feedback =
      Node.div
        [ Attr.id "modal-div"
        ; Attr.classes [ "nes-container"; "modal-content" ]
        ]
        ([ view_question { question with hint = None }
         ; Node.div
             [ Attr.class_ "eval-values" ]
             [ view_value
                 ~eval:report.evaluation
                 "Estimate"
                 question
                 report.estimate
             ; view_value "Actual" question question.value
             ]
         ]
        @ view_context question
        @ [ Node.div
              [ Attr.class_ "accuracy-rating" ]
              [ Node.p [] [ Node.text "Accuracy" ]
              ; VNode.eval_stars ~size:"is-medium" report.evaluation
              ]
          ]
        @ [ next_button inject ])
    in
    let script =
      (* Makes sure we scroll to the top of the modal div, otherwise we end up
         half-way down the modal when it's shown on mobile *)
      Node.create
        "script"
        []
        (* FIXME This is an unfortunate hack :( *)
        [ Node.text
            "setTimeout(function() \
             {document.getElementById('modal-background').scrollTop = 0; },  \
             50)"
        ]
    in
    let modal =
      Node.div
        [ Attr.id "modal-background"; Attr.classes [ "modal-background" ] ]
        [ feedback ]
    in
    let prompt = view_prompt inject None report.question in
    Node.div [] [ prompt; modal; script ]

  let container ?(progress : (int * int) option) nodes =
    let open Vdom in
    let progress_bar =
      match progress with
      | None              -> []
      | Some (value, max) -> [ VNode.progress ~value ~max ]
    in
    Node.div
      [ Attr.id "outer-container" ]
      ([ Node.div
           [ Attr.id "container"
           ; Attr.classes [ "nes-container"; "with-title" ]
           ]
           ([ Node.h1 [ Attr.class_ "title" ] [ Node.text Config.title ] ]
           @ nodes)
       ]
      @ progress_bar)

  let add_param kv uri = Uri.add_query_param' uri kv

  let opt_str s = Option.value s ~default:""

  let view_summary ({ uuid; results; qualtrics_data; _ } : Model.t) =
    let open Vdom in
    let finished =
      let link_url =
        Server_info.return_url
        |> add_param ("uuid", opt_str uuid)
        |> add_param ("cond", condition_id)
        |> add_param ("p", opt_str qualtrics_data.p)
        |> add_param ("vid", opt_str qualtrics_data.vid)
        |> add_param ("id1", opt_str qualtrics_data.id1)
        |> add_param ("id2", opt_str qualtrics_data.id2)
        |> add_param ("id3", opt_str qualtrics_data.id3)
        |> Uri.to_string
      in
      Node.div
        [ Attr.class_ "instruction" ]
        (List.map ~f:(fun l -> Node.p [] [ Node.text l ]) Config.finish_message
        @ [ Node.p
              []
              [ Node.text "To return to the survey, follow "
              ; Node.a [ Attr.href link_url ] [ Node.text "this link" ]
              ; Node.text "."
              ]
          ])
    in
    match results with
    | [] -> Node.div [] [ container [ finished ] ]
    (* No summary if there are no results *)
    | _ ->
        let to_header str =
          Node.td
            [ Attr.class_ "summary-header" ]
            [ Node.strong [] [ Node.text str ] ]
        in
        let to_td el = Node.td [ Attr.class_ "summary-cell" ] [ el ] in
        let row report =
          let Report.{ question; estimate; evaluation; _ } = report in
          let Question.{ value; prompt; units; _ } = question in
          let prompt_cell prompt =
            Node.td
              [ Attr.classes [ "instruction-cell"; "summary-cell" ] ]
              (List.map ~f:(fun t -> Node.p [] [ Node.text t ]) prompt)
          in
          let source_cell =
            let default = [] in
            let f (_, cs) = view_citations cs in
            question.context
            |> Option.value_map ~default ~f
            |> Node.td [ Attr.classes [ "sources-cell"; "summary-cell" ] ]
            |> List.return
          in
          let cells =
            prompt_cell prompt
            ::
            List.map
              ~f:to_td
              [ Node.text units
              ; Node.text (Value.to_string value)
              ; Node.text (Value.to_string estimate)
              ; VNode.eval_stars ~size:"is-medium" evaluation
              ]
            @ source_cell
          in
          Node.(tr [] cells)
        in
        let header =
          Node.thead
            []
            [ Node.tr
                []
                (List.map
                   ~f:to_header
                   [ "Fact"
                   ; "Units"
                   ; "Actual"
                   ; "Estimated"
                   ; "Accuracy"
                   ; "Source"
                   ])
            ]
        in
        let rows = Node.tbody [] (List.rev results |> List.map ~f:row) in
        Node.div
          []
          [ container [ finished ]
          ; Node.div
              [ Attr.classes [ "nes-table-responsive"; "summary-container" ] ]
              [ Node.table
                  [ Attr.classes [ "nes-table"; "is-bordered"; "summary" ] ]
                  [ header; rows ]
              ]
          ]

  let view_instruction inject instruction =
    let open Vdom in
    let f str = Node.p [] [ Node.text str ] in
    Node.div
      [ Attr.class_ "instruction" ]
      (List.map ~f instruction @ [ next_button inject ])

  let compute ~inject () (model : Model.t) =
    let progress =
      let max = model.total_steps in
      (max - List.length model.steps, max)
    in
    let view =
      match model.mode with
      | Mode.Report r -> container ~progress [ view_report inject r ]
      | Mode.Prompt   ->
      match Model.current_step model with
      | None   -> view_summary model (* Out of steps, so done *)
      | Some s ->
      match s with
      | Question q    ->
          container ~progress [ view_prompt inject model.input_err q ]
      | Instruction i -> container ~progress [ view_instruction inject i ]
    in
    scroll_to_top ();
    (model, view)

  (* How to get a Node.t from Tyxml *)
  (* let vdom_elt = Virtual_dom.Tyxml.Html.(toelt (h1 [])) *)
end
