(* We need Uuid.Unstable.t (to avoid deprecation warnings), but
   we also need equal for deriving equal *)

(* So we first include everything in Uuid (which gives us Uuid.equal) *)
include Uuid

(* then shadow with Uuid.Unstable *)
include Uuid.Unstable
