open! Core_kernel

let default_title = "Do you think like a climate scientist?"

module type S = sig
  val name : string
  (** The internal name used for paths etc. *)

  val title : string
  (** The title of the intervention. *)

  val steps : Step.t list
  (** The message to display when the participant completes the session. *)

  val finish_message : string list
  (** The message to display when the participant completes the session. *)
end

let finish_message =
  [ "Thank you for participating! 🌍 "
  ; "When you are ready, please return to the main survey by clicking the link \
     below. Note that you must complete the remainder of the survey to receive \
     compensation."
  ]

let finish_message_summary_paragraph =
  "A summary of your estimates is shown below."

module Example : S = struct
  let name = "example"

  let title = "Estimation Game"

  let steps =
    let open Step in
    [ question
        ~id:"EX-1"
        ~prompt:
          [ "How many shreds of wheat are intertwined in a shredded wheat \
             biscuit?"
          ]
        ~hint:"There are 2000 shreds of wheat in a mini-wheat biscuit"
        ~units:"shreds of wheat"
        ~context:
          ( "This is from a skit on De La Soul's classic album \"3 Feet High \
             and Rising\""
          , [ cite "A Context Source" ~url:"https://google.com"
            ; cite "Another source" ~url:"https://duckduckgo.com"
            ; cite "Prince Paul" ~url:"https://google.com"
            ] )
        (Value.int 10000)
    ; question
        ~id:"EX-2"
        ~prompt:[ "How many is an amount?" ]
        ~hint:"A mountain"
        ~units:"manifolds"
        ~context:("Manifolds fold minimanis. Mountain is a bad pun.", [])
        (Value.int 1)
    ]

  let finish_message = finish_message @ [ finish_message_summary_paragraph ]
end

(** Used in the core condition *)
module Climeate_change_quantities : S = struct
  let title = default_title

  let name = "intervention"

  module Ref = struct
    open Question

    let ranney_2016 =
      { name =
          "Ranney, M., & Clark, D. (2016). Climate change conceptual change: \
           Scientific information can transform attitudes. Topics in Cognitive \
           Science, 8(1), 49–75."
      ; url = Some "https://doi.org/10.1111/tops.12187"
      }

    let thacker_2020 =
      { name =
          "Thacker, I. (2020). Numerical estimation skills, epistemic \
           cognition, and climate change: Mathematical skills and dispositions \
           that can support science learning. In A.I. Sacristán, J.C. \
           Cortés-Zavala & P.M. Ruiz-Arias, (Eds.). Mathematics Education \
           Across Cultures: Proceedings of the Forty-Second Annual Meeting of \
           the North-American Chapter of the International Group for the \
           Psychology of Mathematics Education (pp. 1054-1062). Mazatlán, \
           Sinaloa, Mexico. ISBN: 978-1-7348057-0-3"
      ; url = Some "https://doi.org/10.51272/pmena.42.2020"
      }
  end

  module Units = struct
    let prct = "%"
  end

  let steps =
    let open Step in
    Util.randomized
      [ question
          ~id:"1"
          ~prompt:
            [ "What is the change in atmospheric levels of methane (a \
               greenhouse gas) since 1750 until now?"
            ]
          ~units:Units.prct
          Value.(float_change 151.0 Increase)
          ~context:
            ( "Methane is largely produced by decaying organic material in \
               landfills and dairy manure and can stay in the atmosphere for \
               as long as a few decades. Reducing waste and curbing meat \
               consumption can help to reduce levels of atmospheric methane."
            , [ cite
                  "Chu, J. (2017, January 12). Short-lived greenhouse gases \
                   cause centuries of sea-level rise. NASA Global Climate \
                   Change. Retrieved March 9, 2021"
                  ~url:
                    "https://climate.nasa.gov/news/2533/short-lived-greenhouse-gases-cause-centuries-of-sea-level-rise/"
              ; cite
                  "NASA Global Climate Change. (n.d.a). Climate change: How do \
                   we know? Retrieved March 10, 2021"
                  ~url:"https://climate.nasa.gov/evidence/"
              ; cite
                  "Southern California Gas Company. (n.d.). Sources of methane \
                   emissions. Retrieved March 13, 2021"
                  ~url:
                    "https://www.socalgas.com/stay-safe/methane-emissions/sources-of-methane-emissions"
              ; Ref.ranney_2016
              ] )
      ; question
          ~id:"2"
          ~prompt:
            [ "What is the change in percentage of the world’s ocean ice \
               cover since the 1960s?"
            ]
          ~units:Units.prct
          Value.(float_change 40.0 Decrease)
          ~context:
            ( "Decline in ice cover is particularly troubling because ocean \
               ice reflects the sun’s rays out of the atmosphere. When this \
               ice melts into water, the water more readily absorbs the \
               sun’s rays, further increasing the ocean’s temperature, \
               leading to even more ice melt. Urging governments to take \
               ambitious climate actions may interrupt this feedback loop."
            , [ cite
                  "National Snow & Ice Data Center. (2019, June 18). SOTC: \
                   Mountain glaciers. Retrieved March 10"
                  ~url:"https://nsidc.org/cryosphere/sotc/glacier_balance.html"
              ; cite
                  "Vinas, M. J. (2015, February 11). Study shows global sea \
                   ice diminishing, despite antarctic gains. NASA Global \
                   Climate Change. Retrieved March 10, 2021"
                  ~url:
                    "https://climate.nasa.gov/news/2237/study-shows-global-sea-ice-diminishing-despite-antarctic-gains/"
              ; Ref.ranney_2016
              ] )
      ; question
          ~id:"3"
          ~prompt:
            [ "According to observation data collected at Mauna Loa \
               Observatory in Hawaii, what is the percent change in \
               atmospheric CO₂ levels from 1959 (when observation began) to \
               2009?"
            ]
          ~units:Units.prct
          Value.(float_change 22.6 Increase)
          ~context:
            ( "As of March 4, 2021, levels of CO₂ were 417 parts per million \
               molecules (ppm), 31.8% more than it was at the dawn of the \
               industrial revolution in 1959. Without substantially reducing \
               global carbon emissions, it is estimated that the CO₂ levels \
               will hit 450 ppm by 2040. Conserving energy is one way that \
               individuals can reduce carbon emission and their climate \
               impacts; voting in support of investing in renewable energy is \
               another. "
            , [ cite
                  "NASA Global Climate Change. (n.d.d). The effects of climate \
                   change. Retrieved March 9, 2021"
                  ~url:"https://climate.nasa.gov/effects/"
              ; cite
                  "CO₂-Earth. (n.d.). Earth’s CO₂ home page. Retrieved \
                   March 13, 2021"
                  ~url:"https://www.co2.earth/"
              ; Ref.ranney_2016
              ] )
      ; question
          ~id:"4"
          ~prompt:
            [ "A 2010 article examines the 908 active researchers with at \
               least 20 climate publications on Google Scholar. What \
               percentage of them have stated that it is “very likely” \
               that human-caused emissions are responsible for “most” of \
               the “unequivocal” warming of the earth in the second half \
               of the 20th century?"
            ]
          ~units:"% of researchers"
          Value.(float 97.5)
          ~context:
            ( "Indeed, the overwhelming majority of the leading science \
               organizations have publicly announced that they believe humans \
               to be the leading cause of climate change. This includes the \
               United Nations Intergovernmental Panel on Climate Change and \
               the international and U.S science academies."
            , [ cite
                  "NASA Global Climate Change. (n.d.c). Graphic: carbon \
                   dioxide hits new high. Retrieved March 9, 2021"
                  ~url:
                    "https://climate.nasa.gov/climate_resources/7/graphic-carbon-dioxide-hits-new-high/"
              ; Ref.ranney_2016
              ] )
      ; question
          ~id:"5"
          ~prompt:
            [ "In 1850, there were approximately 150 glaciers present in \
               Glacier National Park. How many are present today?\n"
            ]
          ~units:"glaciers"
          Value.(int 25)
          ~context:
            ( "In 1941, Muir Glacier, a prominent glacier in Glacier National \
               Park, was more than 2,000 feet thick but has significantly \
               melted and vegetation has taken over the areas where the \
               glacier once was. People can help to reduce glacier melt by \
               reducing transportation emissions, such as by flying and \
               driving less often. "
            , [ cite
                  "Weeman, K. (2018, February 13). New study finds sea levels \
                   rise accelerating. NASA Global Climate Change. Retrieved \
                   March 10, 2021"
                  ~url:
                    "https://climate.nasa.gov/news/2680/new-study-finds-sea-level-rise-accelerating/"
              ; cite
                  "National Snow & Ice Data Center. (2019, June 18). SOTC: \
                   Mountain glaciers. Retrieved March 10, 2021"
                  ~url:"https://nsidc.org/cryosphere/sotc/glacier_balance.html"
              ; Ref.ranney_2016
              ] )
      ; question
          ~id:"6"
          ~prompt:
            [ "From 1850 to 2004, what is the percent change of volume of \
               glaciers in the European Alps?"
            ]
          ~units:Units.prct
          Value.(float_change 50.0 Decrease)
          ~context:
            ( "The European Alps are not the only place glaciers have been \
               retreating; satellite evidence shows that glaciers also are \
               receding in the Himalayas, Rockies, Andes, Africa, and Alaska. \
               Reducing climate change can’t happen unless people are aware \
               that it is happening. Starting conversations about climate \
               change can help spread awareness about this important topic."
            , [ cite
                  "AGU. (2019, November). Society must address the growing \
                   climate crisis now. Retrieved March 10, 2021"
                  ~url:
                    "https://www.agu.org/Share-and-Advocate/Share/Policymakers/Position-Statements/Position_Climate"
              ; cite
                  "NASA Global Climate Change. (n.d.a). Climate change: How do \
                   we know? Retrieved March 10, 2021"
                  ~url:"https://climate.nasa.gov/evidence/"
              ; Ref.ranney_2016
              ] )
      ; question
          ~id:"7"
          ~prompt:
            [ "How many inches has the sea level risen or fallen in the last \
               twenty years?"
            ]
          ~hint:
            "global sea levels rose by 1.0 inch between the years of 1900 and \
             1920, near the dawn of the industrial revolution."
          ~units:"Inches"
          Value.(float_change 2.4 Increase)
          ~context:
            ( "By 2100, the sea level is expected to increase by 26 inches at \
               current greenhouse gas emission rates, producing devastation to \
               coastal areas. Swift action to reduce global greenhouse \
               emissions may prevent this sea level rise, such as divesting \
               from fossil fuels and investing in renewable energy."
            , [ cite
                  "Ranney, M., & Clark, D. (2016). Climate change conceptual \
                   change: Scientific information can transform attitudes. \
                   Topics in Cognitive Science, 8(1), 49–75."
              ; cite
                  "Weeman, K. (2018, February 13). New study finds sea levels \
                   rise accelerating. NASA Global Climate Change. Retrieved \
                   March 10, 2021"
                  ~url:
                    "https://climate.nasa.gov/news/2680/new-study-finds-sea-level-rise-accelerating/"
              ; Ref.thacker_2020
              ] )
      ; question
          ~id:"8"
          ~prompt:
            [ "What was the average Arctic Sea ice thickness in meters in 2008?"
            ]
          ~hint:"Arctic ice thickness was 3.64 meters in 1980."
          ~units:"Meters"
          Value.(float 1.89)
          ~context:
            ( "Scientists discovered a 20% decline in the Earth’s ice \
               thickness between 2003 to 2009 when NASA conducted its first \
               ICE Cloud and Land Elevation Satellite-2 (ICESat) mission. \
               People can act to prevent further ice melt by reducing their \
               greenhouse gas emissions and urging policymakers to do the \
               same."
            , [ cite
                  "Chu, J. (2017, January 12). Short-lived greenhouse gases \
                   cause centuries of sea-level rise. NASA Global Climate \
                   Change. Retrieved March 9, 2021"
                  ~url:
                    "https://climate.nasa.gov/news/2533/short-lived-greenhouse-gases-cause-centuries-of-sea-level-rise/"
              ; cite
                  "Ramsayer, K. (2020, May 15). NASA's ICESat-2 measures \
                   arctic ocean's sea ice thickness, snow cover. NASA Global \
                   Climate Change. Retrieved March 9, 2021"
                  ~url:
                    "https://climate.nasa.gov/news/2985/nasas-icesat-2-measures-arctic-oceans-sea-ice-thickness-snow-cover/"
              ; Ref.thacker_2020
              ] )
      ; question
          ~id:"9"
          ~prompt:
            [ "How many square kilometers of Arctic ice cover was there in \
               September of 2017?"
            ]
          ~hint:
            "September ice cover was 6.54 million square kilometers before \
             2010, on average."
          ~units:"Million km²"
          Value.(float 4.8)
          ~context:
            ( "The Earth’s sea ice melt amounts to a loss the size of New \
               Hampshire and Vermont combined, which has been observed yearly \
               since 1996. This discovery was made by Claire Parkinson, a \
               leading climate scientist at NASA's Goddard Space Flight \
               Center. "
            , [ cite
                  "Vinas, M. J. (2015, February 11). Study shows global sea \
                   ice diminishing, despite antarctic gains. NASA Global \
                   Climate Change. Retrieved March 10, 2021"
                  ~url:
                    "https://climate.nasa.gov/news/2237/study-shows-global-sea-ice-diminishing-despite-antarctic-gains/"
              ; Ref.thacker_2020
              ] )
      ; question
          ~id:"10"
          ~prompt:
            [ "How many billions of tons of CO₂ are emitted by the USA each \
               year?"
            ]
          ~hint:
            "European Union, currently consisting of twenty eight countries, \
             collectively emit 3.25 billion tons of CO₂ per year."
          ~units:"Billion tons of CO₂"
          Value.(float 5.0)
          ~context:
            ( "Human activity has caused more than 250 times the amount of \
               CO₂ to be cast into the atmosphere compared to the levels of \
               CO₂ released from natural sources after the last Ice Age. The \
               USA is the world’s second largest emitter of carbon dioxide, \
               after China who produced 10.6 billion metric tons of CO₂ in \
               2018. Concerned citizens can make a difference by voting to \
               reduce accelerating carbon emissions."
            , [ cite
                  "NASA Global Climate Change. (n.d.d). The effects of climate \
                   change. Retrieved March 9, 2021"
                  ~url:"https://climate.nasa.gov/effects/"
              ; cite
                  "Union of Concerned Scientists. (2008, July 16). Each \
                   country’s share of CO₂ emissions. Retrieved March 13, \
                   2021"
                  ~url:
                    "https://www.ucsusa.org/resources/each-countrys-share-co2-emissions"
              ; Ref.thacker_2020
              ] )
      ; question
          ~id:"11"
          ~prompt:
            [ "What is the change in average global temperature in the last 50 \
               years?"
            ]
          ~hint:"the temperature increased by 0.7 °F between 1900 and 1950."
          ~units:"°F"
          Value.(float_change 1.62 Increase)
          ~context:
            ( "1.6 °F may not seem like a dramatic temperature increase \
               relatively speaking, but on a global scale, even a half a \
               degree increase is expected to lead to more intense heat waves, \
               rain storms, sea level rise, depletion of agricultural \
               resources, and destruction of the coral reefs. Leading \
               scientists and policymakers have joined forces to map out a \
               plan to prevent global temperatures from exceeding 3.6°F \
               (2°C), though swift action and pressure from citizens is \
               needed for it to be successful."
            , [ cite
                  "Ramsayer, K. (2020, May 15). NASA's ICESat-2 measures \
                   arctic ocean's sea ice thickness, snow cover. NASA Global \
                   Climate Change. Retrieved March 9, 2021"
                  ~url:
                    "https://climate.nasa.gov/news/2985/nasas-icesat-2-measures-arctic-oceans-sea-ice-thickness-snow-cover/"
              ; cite
                  "Silberg, B. (2016, June 29). Why a half-degree temperature \
                   rise is a big deal. NASA Global Climate Change. Retrieved \
                   March 13, 2021"
                  ~url:
                    "https://climate.nasa.gov/news/2458/why-a-half-degree-temperature-rise-is-a-big-deal/#:~:text=But%20at%202%20C%2C%20the,that%20water%20deficit%20nearly%20doubles."
              ; Ref.thacker_2020
              ] )
      ; question
          ~id:"12"
          ~prompt:
            [ "Of 195 countries in the world, how many are committed to \
               climate action?"
            ]
          ~units:"countries"
          Value.(int 175)
          ~context:
            ( "An overwhelming majority of countries have committed to take \
               steps toward limiting the increase of the Earth’s temperature \
               increase to 2.7 degrees F (1.5 degrees C). While most countries \
               are not currently meeting their climate pledges, fast and \
               concerted action can improve the climate impact on people’s \
               health, the economy, society, and stress our world's \
               ecosystems."
            , [ cite
                  "NASA Global Climate Change. (n.d.b). Do scientists agree on \
                   climate change?  Retrieved March 10, 2021"
                  ~url:
                    "https://climate.nasa.gov/faq/17/do-scientists-agree-on-climate-change/"
              ; cite
                  "Mulvaney, K. (2019, September 19). Climate change report \
                   card: These countries are reaching targets. National \
                   Geographic. Retrieved March 13, 2021"
                  ~url:
                    "https://www.nationalgeographic.com/environment/article/climate-change-report-card-co2-emissions"
              ; Ref.thacker_2020
              ] )
      ]

  let finish_message = finish_message @ [ finish_message_summary_paragraph ]
end

(** The climate change quantities prepended with a modification to provide
    instructions and two practice problems. *)
module Modification : S = struct
  let title = default_title

  let name = "modification"

  let instruction_and_practice =
    let open Step in
    [ instruction
        [ "Please read the text on the following page carefully. The text will \
           help you to complete the activities that follow."
        ]
    ; instruction
        [ "ROUGH ESTIMATES"
        ; "Sometimes people estimate everyday numbers in their head. For \
           example, you might quickly estimate the cost of tax and tip in your \
           head before ordering a meal at a restaurant. These calculations are \
           naturally very rough and imprecise,and it is okay if your guess is \
           not perfect. In this activity you will estimate many numbers, and \
           when you do, you should try to feel comfortable if your estimates \
           are not perfectly correct. Before we start, let's learn a few \
           strategies to improve our estimation accuracy."
        ]
    ; question
        ~id:"13"
        ~practice:true
        ~prompt:
          [ "REFERENCE NUMBERS"
          ; "Numbers that you already know (reference numbers) can help you \
             estimate numbers that you do not know. For example, if you know \
             that about 300 pennies fit in a small, 8oz milk carton, you can \
             use this information to estimate the number of pennies that fit \
             in a gallon. Let’s say that about 20 milk cartons fit in a \
             gallon, thus 20 × 300 = 6,000 pennies fit into the gallon \
             container."
          ; "Now you try: What is the number of pennies that fit inside of a \
             tennis ball?"
          ]
        ~hint:"53 pennies fit inside of a ping-pong ball."
        ~units:"pennies"
        ~context:
          ( "After each estimate, we will share some background information \
             about the quantity that you estimated. This gives context to the \
             importance of the number."
          , [ cite
                "We will provide citations for the facts here. You can explore \
                 them if you want. But it is purely optional."
            ] )
        Value.(int 212)
    ; question
        ~id:"14"
        ~practice:true
        ~prompt:
          [ "SIMPLIFYING NUMBERS"
          ; "When using reference numbers, you may want to round values to \
             make mental computation easier. For example, let’s estimate the \
             population of California given that the population of Kentucky is \
             4.47 million. Before making our estimate, we first round the \
             Kentucky population to 4 million to make the math easier, and \
             scale this value according to our beliefs about the size of \
             California compared to Kentucky. If you were to guess that \
             California is about ten times more populous than Kentucky, you \
             might then estimate that California has 10 × 4 million = 40 \
             million people."
          ; "Try another one: What is the average weight in pounds of garbage \
             produced per person in the USA every day?"
          ]
        ~hint:"an uneaten apple typically weights 0.33 pounds."
        ~units:"lbs"
        ~context:
          ( "In addition to the context of numbers, you will receive feedback \
             on accuracy of your estimate as shiny stars. The purpose of this \
             feedback is to encourage you to think about the values and the \
             facts. The accuracy of your estimate does not reflect how well \
             you are doing. Remember, all estimates are imperfect! If you have \
             two or more stars, it means your estimate is in the right \
             direction, and the more stars you see the more accurate it is."
          , [ cite
                "We will provide citations for the facts here. You can explore \
                 them if you want. But it is purely optional."
            ] )
        Value.(float 5.0)
    ; instruction
        [ "You will now estimate various real-world numbers. After estimating \
           each quantity, you will be shown the scientifically accepted value."
        ; "At the end of the exercise, you will be able to view a report on \
           the accuracy of all of your estimates."
        ]
    ]

  let steps = instruction_and_practice @ Climeate_change_quantities.steps

  let finish_message = finish_message @ [ finish_message_summary_paragraph ]
end

(** The conrol has no steps, and just a single instructional text. *)
module Control : S = struct
  let title = default_title

  let name = "control"

  let steps =
    let open Step in
    [ instruction
        [ "THE ENHANCED GREENHOUSE EFFECT"
        ; "Many people have heard of the “greenhouse effect”, but not \
           everyone knows what the “greenhouse effect” is exactly. The \
           greenhouse effect refers to the way that certain gases in earth’s \
           atmosphere keep the planet warmer than it would otherwise be. The \
           earth’s greenhouse effect is a natural occurrence that helps \
           raise our planet’s average temperature, making it habitable. \
           Without naturally occurring greenhouse gases like water vapor, \
           carbon dioxide, and methane, more of Earth’s energy would radiate \
           back into space and Earth’s average temperature would be about \
           -1°F, which is about 60°F colder than it is today. Life on Earth \
           would be much different without a greenhouse effect. In fact, life \
           might not exist on Earth at all without the greenhouse effect."
        ; "So how does the greenhouse effect work? Energy in the form of \
           visible light from the sun enters Earth’s atmosphere. Clouds and \
           other particles in the atmosphere reflect about 26% of this solar \
           energy back into space. Some of this incoming solar energy (about \
           19%) is absorbed by clouds, gases, and other atmospheric particles. \
           But because the atmosphere is mostly transparent to visible light, \
           about 55% of the solar energy passes through the atmosphere and \
           reaches Earth’s surface. A small portion of this solar energy \
           (about 4%) is reflected back into space by the earth’s surface. \
           But most (51%) of the energy from the sun is absorbed by the \
           earth’s land and oceans. An analogy may help illustrate this \
           process."
        ; "Imagine your car parked out in the sun with the windows slightly \
           open. The temperature inside your car feels warmer than the \
           outside temperature. The reason for this difference in temperature \
           is that the sun’s light energy enters through the car windows and \
           is transferred to the seats, dashboard, carpeting, and floor mats. \
           These objects re-radiate some of this energy in a form of invisible \
           light, called infrared. Windows are opaque to and block this \
           infrared light, causing the energy to be trapped inside the car. \
           Some of the blocked energy is transferred to the air inside the \
           car, raising its temperature. This is an example of a greenhouse \
           effect. Similarly, Earth is covered by a blanket of gases, which, \
           like the windows on the car, allow light energy from the sun to \
           pass through to the earth’s surface, but block some infrared \
           light from returning to space. "
        ; "The naturally occurring greenhouse effect keeps the earth \
           habitable, but an enhanced greenhouse effect is responsible for \
           current global warming. Scientific observations confirm that the \
           average global temperature is rising. Surface temperatures have \
           increased since 1880, with most of the warming occurring since the \
           1970s. Moreover, the twenty warmest years on record have occurred \
           since 1981 with the top ten taking place in the last twelve years. \
           The rapid warming that the earth is currently experiencing cannot \
           be explained by natural factors alone. The climate can and is being \
           changed by humans. In fact, almost all climate scientists (97%) \
           agree that human activities have added to the natural greenhouse \
           effect. As the amount of greenhouse gases in the atmosphere \
           increases, more infrared light is absorbed and re-radiated, in \
           turn; the earth’s surface is further warmed. The additional \
           warming of Earth due to increased greenhouse gases concentrations \
           caused by human activities (such as the burning of fossil fuels) is \
           called the enhanced greenhouse effect. It is this human enhanced \
           greenhouse effect that is causing global warming. To continue the \
           car analogy, humans have the ability to influence the windows on \
           the car, and we have been slowly rolling the windows up. "
        ; "The main greenhouse gases entering the atmosphere through human \
           activities include carbon dioxide, methane, nitrous oxide, and \
           fluorinated gases. The increase in carbon dioxide in the atmosphere \
           comes mainly from burning fossil fuels (coal, oil, and natural gas) \
           and by deforestation (the clearing of forests which naturally \
           absorb carbon dioxide). Methane is given off when coal and oil is \
           produced and transported, by raising livestock and other \
           agricultural practices, and through the decay of organic waste in \
           landfills. Nitrous oxide is emitted into the atmosphere through \
           industrial and agricultural practices, as well as through the \
           combustion of fossil fuels and solid waste. Fluorinated gases are \
           also being released into the atmosphere through industrial \
           processes and are contributing to the enhanced greenhouse effect. \
           Although all of these gases contribute to the enhanced greenhouse \
           effect, carbon dioxide is the gas you probably have heard about the \
           most because it accounts for more than 60% of the enhanced \
           greenhouse effect. Most of the U.S. carbon dioxide emissions result \
           from fossil fuel combustion used to generate electric power and \
           exhaust from cars, trucks, airplanes, and trains. We all know the \
           warnings about leaving a child or pet in a locked car with the \
           windows up on a sunny day. If global warming is not mitigated, we \
           run the risk of Earth’s temperature rising to unsafe levels."
        ]
    ]

  let finish_message = finish_message
end
