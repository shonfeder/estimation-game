open! Core_kernel
open Bonsai_web

module Make (C : Config.S) = struct
  (** [Make (Config)] is an implementation of the client code, configured by the
    supplied [Config]. *)

  module Component = Component.Make (C)

  let estimation_component =
    let default_model = Model.default C.steps in
    Bonsai.of_module ~default_model (module Component)

  let application : (Component.Input.t, Vdom.Node.t) Bonsai.t =
    let open Bonsai.Let_syntax in
    let%map _model, v = estimation_component in
    Vdom.Node.div [] [ v ]
end
