open! Core_kernel

type citation =
  { name : string
  ; url : string option
  }
[@@deriving sexp, equal]

type context = string * citation list [@@deriving sexp, equal]

let cite ?url name = { name; url }

type t =
  { id : string
  ; prompt : string list
  ; practice : bool
  ; value : Value.t
  ; units : string
  ; hint : string option
  ; context : context option
  }
[@@deriving sexp, equal]

exception Duplicate_question_ids of string

let ids = ref (Set.empty (module String))

let make ~id ~prompt ~units ?(practice = false) ?hint ?context value =
  if Set.mem !ids id then
    failwith
      (Printf.sprintf "Question IDs must be unique. Found duplicate ID %s" id);
  ids := Set.add !ids id;
  { id; prompt; practice; value; units; hint; context }
