(** [Report] reports on the result of an estimation *)

open! Core_kernel

type t =
  { question : Question.t
  ; evaluation : Evaluation.t
  ; estimate : Value.t
  ; condition : string
  ; uuid : string option
  }
[@@deriving sexp, equal]

let of_string t =
  match Sexp.of_string_conv t t_of_sexp with
  | `Error (exn, _) -> Error (`Msg (Exn.to_string exn))
  | `Result r       -> Ok r
