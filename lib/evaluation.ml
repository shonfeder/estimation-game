open! Core_kernel

type t =
  | Wrong_dir
  | Very_bad
  | Bad
  | Good
  | Very_good
[@@deriving sexp, equal]

let to_class_string = function
  | Wrong_dir -> "eval-wrong-dir"
  | Very_bad  -> "eval-v-bad"
  | Bad       -> "eval-bad"
  | Good      -> "eval-good"
  | Very_good -> "eval-v-good"

let to_string = function
  | Wrong_dir -> "Wrong direction"
  | Very_bad  -> "Very inaccurate"
  | Bad       -> "Inaccurate"
  | Good      -> "Less accurate"
  | Very_good -> "Very accurate"

let of_string = function
  | "Wrong direction" -> Wrong_dir
  | "Very inaccurate" -> Very_bad
  | "Inaccurate"      -> Bad
  | "Less accurate"   -> Good
  | "Very accurate"   -> Very_good
  | _                 -> failwith "invalid Evaluation.t"

let max : t = Very_good
let min : t = Wrong_dir

let to_int : t -> int = function
  | Wrong_dir -> 1
  | Very_bad  -> 2
  | Bad       -> 3
  | Good      -> 4
  | Very_good -> 5

type sign =
  | Pos
  | Neg
[@@deriving equal]

let float_sign v =
  if Float.(v < 0.0) then
    Neg
  else
    Pos

(** [evaluate value estimate] is a score of the accuracy of the [estimate]
    relative to the true [value].

    To evaluate an answer we calculate the proportional difference of the
    estimate from the true value and use that to find the score. If the estimate
    is within 10% of the true value, the score is 3, within 20% score is 2,
    within 30% score is 1, and beyond that a score of 0. Since a good guess
    could be within 10% above or below the true value, take the absolute value
    of the proportion so you don't have to deal with negative numbers: *)
let evaluate actual estimate =
  let actual, estimate = (Value.to_float actual, Value.to_float estimate) in
  if not (equal_sign (float_sign actual) (float_sign estimate)) then
    Wrong_dir
  else
    let open Float in
    let p = abs (abs (estimate - actual) / actual) in
    if p <= 0.1 then
      Very_good
    else if p <= 0.2 then
      Good
    else if p <= 0.3 then
      Bad
    else
      Very_bad

let%expect_test "evaluate ints" =
  let eval estimate =
    evaluate (Value.int 10000) estimate |> to_string |> Printf.printf "%s"
  in
  eval (Value.int 9000);
  [%expect {| Very accurate |}];
  eval (Value.int 8000);
  [%expect {| Less accurate |}];
  eval (Value.int 7000);
  [%expect {| Inaccurate |}];
  eval (Value.int 4000);
  [%expect {| Very inaccurate |}]

let%expect_test "evaluate float change" =
  let eval estimate =
    evaluate Value.(float_change 50. Decrease) estimate
    |> to_string
    |> Printf.printf "%s"
  in
  eval Value.(float_change 45. Decrease);
  [%expect {| Very accurate |}];
  eval Value.(float_change 40. Decrease);
  [%expect {| Less accurate |}];
  eval Value.(float_change 35. Decrease);
  [%expect {| Inaccurate |}];
  eval Value.(float_change 123. Decrease);
  [%expect {| Very inaccurate |}];
  eval Value.(float_change 10. Increase);
  [%expect {| Wrong direction |}]
