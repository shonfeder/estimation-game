(* Interventions and Experimental Conditions. Between pretest and posttest,
   participants will be randomly assigned to one of three conditions: (1) a
   control group in which participants will be presented with a 817 word
   expository text about the greenhouse effect adapted from (Lombardi et al.,
   2013), (2) the estimation game developed from Phase 1of this study, where
   learners estimate climate change-related quantities before being presented
   with the scientifically accepted answer (see Table 1 for examples); or (3)
   the estimation game accompanied with numerical estimation instruction—an
   instructional text designed to present learners with strategies for using the
   given “hints” to better estimate the unknown quantities (see Figure 2 for a
   summary of conditions). The numeracy instruction will also include a worked
   example and two checks for understanding (see Table 1 for examples). *)

(* 1. Prompted to estimate number - E.g., how many shreds of wheat intertwined
   in a shredded wheat biscuit - Provide hint: (There are 2000 shreds of wheat
   in a mini-wheat biscuit) 2. Make guess 3. Presents true value - Include
   citation from where the value comes from - Records the “fact of the matter”,
   e.g., the evidence 4. Scoring - With a “percentage of accuracy”. E.g. -
   Within 10% is very good - Within 30% is pretty good - Otherwise very bad -
   visual indicator. E.g. - screen green if correct - Yellow if close - Red if
   an order of magnitude off - Track running score *)

(* TODO Refactor so we don't need to expose so many internal modules *)
module Report = Report
module Uuid_util = Uuid_util
module Value = Value
module Evaluation = Evaluation

module Config = Config
module Server_info = Server_info
module Impl = Impl

let version = "0.4.2"
