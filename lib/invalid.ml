open! Core_kernel

type t =
  | Float of string
  | Int of string
  | No_value
  | No_direction
[@@deriving sexp, equal]

let to_string = function
  | Float _      -> "Quantity must be a number."
  | Int _        -> "Quantity must be an integer."
  | No_value     -> "An estimate must be made."
  | No_direction -> "Direction must be choosen."
