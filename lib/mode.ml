open! Core_kernel

(** [Mode] represents the modes of interaction *)

type t =
  | Prompt
  | Report of Report.t
[@@deriving sexp, equal]
