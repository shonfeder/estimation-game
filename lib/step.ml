open! Core_kernel

type t =
  | Question of Question.t
  | Instruction of string list
[@@deriving sexp, equal]

let question ~id ~prompt ~units ?practice ?hint ?context value =
  Question (Question.make ~id ~prompt ~units ?practice ?hint ?context value)

let cite = Question.cite

let instruction lines = Instruction lines
