open! Core_kernel

module Change = struct
  type t =
    | Increase
    | Decrease
  [@@deriving sexp, equal]

  let to_string = function
    | Increase -> "increase"
    | Decrease -> "decrease"

  let of_string = function
    | "increase" -> Some Increase
    | "decrease" -> Some Decrease
    | _          -> None

  let to_float_sign = function
    | Increase -> fun f -> +.f
    | Decrease -> fun f -> -.f
end

module Quantity = struct
  type t =
    | Int of int
    | Float of float
  [@@deriving sexp, equal]

  let to_float = function
    | Int i   -> Int.to_float i
    | Float f -> f

  let to_string = function
    | Int i   -> Int.to_string i
    | Float f -> Printf.sprintf "%.2f" f

  let of_string _ = failwith "Quantity.of_string is unsupported"
end

type t =
  { quantity : Quantity.t
  ; change : Change.t option
  }
[@@deriving sexp, equal]

let make ?dir quantity = { quantity; change = dir }

let int x = make (Int x)

let float x = make (Float x)

let int_change i dir = make (Int i) ~dir

let float_change f dir = make (Float f) ~dir

let with_change dir v = { v with change = Some dir }

let to_float { quantity; change } =
  let f = Quantity.to_float quantity in
  match change with
  | None     -> f
  | Some dir -> (Change.to_float_sign dir) f

let to_string { quantity; change } =
  let q = Quantity.to_string quantity in
  match change with
  | None   -> q
  | Some d ->
      let dir = Change.to_string d in
      [%string {|%{q}%{"%"} %{dir}|}]

let float_of_string str =
  try Ok (float (Float.of_string str)) with
  | Invalid_argument err -> Error (Invalid.Float err)

let int_of_string str =
  try Ok (int (Int.of_string str)) with
  | Failure err -> Error (Invalid.Int err)

let%expect_test "render values" =
  print_string (to_string (int 2));
  [%expect {|2|}];
  print_string (to_string (float 2.0));
  [%expect {|2.00|}];
  print_string (to_string (make (Float 10.) ~dir:Increase));
  [%expect {|10.00% increase|}];
  print_string (to_string (make (Int 12) ~dir:Decrease));
  [%expect {|12% decrease|}]
