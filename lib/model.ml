open! Core_kernel

(* We don't know what this data is for, but Qualtrics apparently
 * needs it to coordinate two different surveys. *)
type qualtrics_data =
  { p : string option
  ; vid : string option
  ; id1 : string option
  ; id2 : string option
  ; id3 : string option
  }
[@@deriving sexp, equal]

type t =
  { steps : Step.t List.t
  ; total_steps : int
  ; results : Report.t List.t
  ; mode : Mode.t
  ; input_err : String.t Option.t
  ; uuid : string option
  ; qualtrics_data : qualtrics_data
  }
[@@deriving sexp, equal]
(** [Model] represents the state of the interaction *)

let default steps =
  let total_steps = List.length steps in
  { steps
  ; total_steps
  ; mode = Mode.Prompt
  ; results = []
  ; input_err = None
  ; uuid = None
  ; qualtrics_data =
      { p = None; vid = None; id1 = None; id2 = None; id3 = None }
  }

let current_step t =
  match t.steps with
  | []     -> None
  | q :: _ -> Some q

(** [current_question model] is [Some q] if [q] is the next step and it is a
    question, otherwise it is [None]. *)
let current_question t =
  match t.steps with
  | Question q :: _ -> Some q
  | _               -> None

(** [next_question model] is [Some (q, model')] if [q] is the next question
    in [model] and [model'] is the model with that question removed from its
    queue, or else [None]. *)
let next_question t =
  match t.steps with
  | Question q :: steps -> Some (q, { t with steps })
  | _                   -> None

let set_mode mode model = { model with mode }

let step t =
  let mode = Mode.Prompt in
  match t.steps with
  | _ :: steps -> { t with mode; steps }
  | empty      -> { t with mode; steps = empty }

let record_estimate condition estimate model =
  match current_question model with
  | None          -> model (* TODO This should actually be an error condition *)
  | Some question ->
      let result =
        let evaluation = Evaluation.evaluate question.value estimate in
        Report.{ estimate; question; evaluation; condition; uuid = model.uuid }
      in
      let results =
        if question.practice then
          (* Don't add practice questions to the results *)
          model.results
        else
          result :: model.results
      in
      let mode = Mode.Report result in
      { model with results; mode }
