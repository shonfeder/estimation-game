open! Core_kernel

(* See
   https://discuss.ocaml.org/t/more-natural-preferred-way-to-shuffle-an-array/217/6?u=shonfeder *)

(** Randomizes the order of questiona *)
let rec randomized = function
  | []         -> []
  | [ single ] -> [ single ]
  | list       ->
      let before, after = List.partition_tf ~f:(fun _ -> Random.bool ()) list in
      List.rev_append (randomized before) (randomized after)
