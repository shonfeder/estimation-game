let port =
  let default = 8888 in
  "PORT"
  |> Sys.getenv_opt
  |> Option.map (fun s -> int_of_string s)
  |> Option.value ~default

let host =
  let default = "localhost" in
  "HOST" |> Sys.getenv_opt |> Option.value ~default

let return_url = "https://utsa.az1.qualtrics.com/jfe/form/SV_51heEkn5uJHfRrg"

(** The host of the server where the app is served *)
let serv =
  let default = "localhost" in
  "SERV" |> Sys.getenv_opt |> Option.value ~default

let () =
  Printf.printf {|
let port = %i
|} port;
  Printf.printf {|
let host = "%s"
|} host;
  Printf.printf {|
let return_url = Uri.of_string "%s"
|} return_url;
  Printf.printf {|
let serv = "%s"
|} serv;
  Printf.printf {|
let password = "3141"
|}
