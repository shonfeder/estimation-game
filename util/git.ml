open! Bos_setup

let is_dry = ref false

let run_status cmd =
  if !is_dry then
    let () = Cmd.to_string cmd |> print_endline in
    Ok (`Exited 0)
  else
    OS.Cmd.run_status cmd

let run cmd =
  if !is_dry then
    Cmd.to_string cmd |> print_endline |> Result.ok
  else
    OS.Cmd.run cmd

let url =
  "git@gitlab.com:conceptual-change-research/estimation-game-results.git"

let git = Cmd.v "git"

let push = Cmd.(git % "push" % "origin" % "main")

let clone = Cmd.(git % "clone" % url % ".")

(* let add_remote = Cmd.(git % "remote" % "add" % "origin" % url) *)

let add_all = Cmd.(git % "add" % ".")

let is_unchanged =
  Cmd.(git % "diff-index" % "--name-status" % "--exit-code" % "HEAD")

let commit msg = Cmd.(git % "commit" % "-m" % msg)

let ( let* ) = Result.bind

let repo =
  let report_dir = Fpath.v Reporter.file |> Fpath.parent in
  let cwd = OS.Dir.current () |> Rresult.R.get_ok in
  Fpath.(cwd // report_dir)

let clone_repo () =
  print_endline "cloning repo";
  run clone

let ensure_initalized () =
  let* git_dir_exists = OS.Dir.exists (Fpath.v ".git") in
  if git_dir_exists then
    Ok ()
  else
    clone_repo ()

let update () =
  let* () = run add_all in
  let* is_changed =
    let* status = run_status is_unchanged in
    match status with
    | `Exited 0 -> Ok false
    | `Exited 1 -> Ok true
    | _         ->
        Error
          (`Msg
            (Printf.sprintf
               "invalid status from %s"
               (Cmd.to_string is_unchanged)))
  in
  if is_changed then (
    let* () = run (commit "Updating responses data") in
    print_endline "updating results";
    run push
  ) else (
    print_endline "no results to update";
    Ok ()
  )

let () =
  if Array.length Sys.argv > 1 && Sys.argv.(1) = "dry" then (
    is_dry := true;
    print_endline "dry run"
  ) else
    print_endline "updating report";
  let f () =
    Rresult.R.failwith_error_msg
    @@ let* () = ensure_initalized () in
       update ()
  in
  Rresult.R.failwith_error_msg @@ OS.Dir.with_current repo f ()
